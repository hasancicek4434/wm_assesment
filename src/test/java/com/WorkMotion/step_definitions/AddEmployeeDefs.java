package com.WorkMotion.step_definitions;

import com.WorkMotion.pages.AddEmployeePage;
import com.WorkMotion.pages.MainPage;
import com.WorkMotion.utilities.BrowserUtils;
import com.WorkMotion.utilities.ConfigurationReader;
import com.WorkMotion.utilities.Driver;
import com.github.javafaker.Faker;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class AddEmployeeDefs {

    WebDriver driver= Driver.get();

    MainPage page= new MainPage();
    AddEmployeePage addPage=new AddEmployeePage();

    Faker faker= new Faker();
    String firstName= faker.name().firstName();
    String lastName= faker.name().lastName();
    String fullName= firstName+" "+lastName;
    String employeeEmail= firstName+"_"+lastName+"@"+"tester.com";


    @Given("logged in as HR manager")
    public void logged_in_as_HR_manager() {

        driver.get(ConfigurationReader.get("url"));
        BrowserUtils.waitForPageToLoad(20);

        page.logIn();
    }

    @When("the user Add new employee with all steps")
    public void the_user_Add_new_employee_with_all_steps() {

        page.addEmployee.click();
        addPage.createNewItem.click();

        BrowserUtils.waitForVisibility(addPage.selectCountry,10);
        addPage.selectCountry.click();
        BrowserUtils.waitFor(1);

        addPage.selectCountry.sendKeys(Keys.ENTER);
        BrowserUtils.waitFor(2);

        addPage.getStarted.click();
        BrowserUtils.waitForVisibility(addPage.inputbox.get(0),10);

        addPage.inputbox.get(0).sendKeys(firstName);
        addPage.inputbox.get(1).sendKeys(lastName);
        BrowserUtils.waitFor(2);

        addPage.checkbox.get(0).click();
        addPage.checkbox.get(3).click();

        addPage.inputbox.get(2).sendKeys("QA ENGINEER");
        driver.findElement(By.xpath("//*[@*='sc-jObWnj kzOhHR']")).sendKeys("Software Testing");
        BrowserUtils.waitFor(1);

        addPage.checkbox.get(4).click();
        BrowserUtils.waitFor(1);

        addPage.calendar.click();
        BrowserUtils.waitFor(1);
        driver.findElement(By.xpath("//*[@*='Next Month']")).click();
        BrowserUtils.waitFor(1);

        driver.findElement(By.xpath("//*[@*='Choose Friday, February 25th, 2022']")).click();
        BrowserUtils.waitFor(1);

        addPage.continueButton.click();
        BrowserUtils.waitFor(3);

        addPage.continueButton.click();
        BrowserUtils.waitFor(3);

        addPage.salary.sendKeys("5000");
        addPage.continueButton.click();
        BrowserUtils.waitFor(3);

        addPage.email.sendKeys(employeeEmail);
        addPage.continueButton.click();
        BrowserUtils.waitFor(3);
    }

    @Then("the user should be able to start employee onboarding")
    public void the_user_should_be_able_to_start_employee_onboarding() {

        addPage.confirmBox.click();
        addPage.finishButton.click();
        BrowserUtils.waitForVisibility(driver.findElement(By.xpath("//*[@*='sc-JkixQ jJtLhE']")),20);
        BrowserUtils.waitFor(2);

        Assert.assertTrue(driver.findElement(By.xpath("//*[@*='sc-JkixQ jJtLhE']")).isDisplayed());
    }

    @When("the user checks the action items section after confirming the employee onboarding")
    public void the_user_checks_the_action_items_section_after_confirming_the_employee_onboarding() {

        page.actionItems.click();
        BrowserUtils.waitFor(8);
        driver.navigate().refresh();
        BrowserUtils.waitForVisibility(page.markAsDone,20);
    }

    @Then("the user should be able to see employee as pending")
    public void the_user_should_be_able_to_see_employee_as_pending() {

        String pendingTitle= page.actionsTitle.get(0).getText();
        Assert.assertTrue(pendingTitle.contains(fullName));
    }

    @When("the user mark the action that been received")
    public void the_user_mark_the_action_that_been_received() {

        page.markAsDone.click();
        BrowserUtils.waitFor(2);
        BrowserUtils.waitForVisibility(page.retrieve,20);
        BrowserUtils.hover(page.retrieve);
    }

    @Then("the user should be able to assert that its been done")
    public void the_user_should_be_able_to_assert_that_its_been_done() {

        BrowserUtils.hover(page.doneTitle.get(0));
        String doneTitle= page.doneTitle.get(0).getText();
        Assert.assertTrue(doneTitle.contains(fullName));
        BrowserUtils.waitFor(2);
    }

}
