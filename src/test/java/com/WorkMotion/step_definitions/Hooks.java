package com.WorkMotion.step_definitions;

import com.WorkMotion.utilities.ConfigurationReader;
import com.WorkMotion.utilities.Driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.concurrent.TimeUnit;

public class Hooks {

    @Before
    public void setUp(){

        Driver.get().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        String browser= ConfigurationReader.get("browser");

        if(browser.equals("chrome-headless")){
            Driver.get().manage().window().setSize(new Dimension(1440, 900));
        }else if(browser.equals("chrome")||browser.equals("firefox")||browser.equals("safari")||browser.equals("edge")){
            Driver.get().manage().window().maximize();
        }

    }


    @After
    public void tearDown(Scenario scenario){
        if(scenario.isFailed()){
            final byte[] screenshot = ((TakesScreenshot) Driver.get()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/png","screenshot");
        }

       Driver.closeDriver();

    }

}
