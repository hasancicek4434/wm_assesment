package com.WorkMotion.pages;


import com.WorkMotion.utilities.BrowserUtils;
import com.WorkMotion.utilities.ConfigurationReader;
import com.WorkMotion.utilities.Driver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public abstract class  BasePage {

    @FindBy(id = "email")
    public WebElement email;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(xpath = "//*[@*='submit']")
    public WebElement loginButton;

    @FindBy(xpath = "//*[@*='add-employee-menu']")
    public WebElement addEmployee;

    @FindBy(id = "sidebar-action-items-link")
    public WebElement actionItems;

    @FindBy(xpath = "(//*[@*='action-items-mark-done-btn'])[1]")
    public WebElement markAsDone;

    @FindBy(xpath = "(//*[text()='Retrieve'])[1]")
    public WebElement retrieve;

    @FindBy(xpath = "//*[@*='max-width: 500px; overflow-wrap: break-word;']")
    public List<WebElement> actionsTitle;

    @FindBy(xpath = "//*[text()='Done']/parent::div/preceding-sibling::span")
    public List<WebElement> doneTitle;

    public BasePage() {
        PageFactory.initElements(Driver.get(), this);
    }



    public void logIn(){

        email.sendKeys(ConfigurationReader.get("username"));
        BrowserUtils.waitFor(1);

        password.sendKeys(ConfigurationReader.get("pass"));
        BrowserUtils.waitFor(1);

        BrowserUtils.doubleClick(loginButton);
        BrowserUtils.waitForClickability(addEmployee,20);
    }


}
