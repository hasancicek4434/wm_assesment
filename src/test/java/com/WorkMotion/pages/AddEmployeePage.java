package com.WorkMotion.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class AddEmployeePage extends BasePage{

    @FindBy(xpath = "//*[@*='create-new-item']")
    public WebElement createNewItem;

    @FindBy(xpath = "//*[@*='react-select-2-input']")
    public WebElement selectCountry;

    @FindBy(id = "onboarding-get-started-btn")
    public WebElement getStarted;

    @FindBy(xpath = "//*[@*='sc-giYglK cmoRFA']")
    public List<WebElement> checkbox;

    @FindBy(xpath = "//*[@*='sc-cCcXHH jvTQHa']")
    public List<WebElement> inputbox;

    @FindBy(xpath = "//*[@*='react-datepicker__input-container']")
    public WebElement calendar;

    @FindBy(id = "onboarding-continue-btn")
    public WebElement continueButton;

    @FindBy(xpath = "//*[@*='e.g. 4000']")
    public WebElement salary;

    @FindBy(xpath = "//*[@*='example@email.com']")
    public WebElement email;

    @FindBy(xpath = "//*[@*='sc-fKVqWL kEkdBF']")
    public WebElement confirmBox;

    @FindBy(id = "onboarding-finish-btn")
    public WebElement finishButton;


}
