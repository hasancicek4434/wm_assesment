@smoke
Feature: Add Employee process

  Scenario: HR Manager should be able to add new employee
    Given logged in as HR manager
    When the user Add new employee with all steps
    Then the user should be able to start employee onboarding
    When the user checks the action items section after confirming the employee onboarding
    Then the user should be able to see employee as pending
    When the user mark the action that been received
    Then the user should be able to assert that its been done
