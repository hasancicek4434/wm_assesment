$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/AddEmployee.feature");
formatter.feature({
  "name": "Add Employee process",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "HR Manager should be able to add new employee",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "logged in as HR manager",
  "keyword": "Given "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.logged_in_as_HR_manager()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user Add new employee with all steps",
  "keyword": "When "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_Add_new_employee_with_all_steps()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user should be able to start employee onboarding",
  "keyword": "Then "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_should_be_able_to_start_employee_onboarding()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user checks the action items section after confirming the employee onboarding",
  "keyword": "When "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_checks_the_action_items_section_after_confirming_the_employee_onboarding()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user should be able to see employee as pending",
  "keyword": "Then "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_should_be_able_to_see_employee_as_pending()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user mark the action that been received",
  "keyword": "When "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_mark_the_action_that_been_received()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user should be able to assert that its been done",
  "keyword": "Then "
});
formatter.match({
  "location": "com.WorkMotion.step_definitions.AddEmployeeDefs.the_user_should_be_able_to_assert_that_its_been_done()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});